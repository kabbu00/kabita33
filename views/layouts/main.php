<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
   <header>
            <div class="container-fluid p-0">
                <nav class="navbar navbar-expand-lg">
                    <a class="nav-link" href="#">
                    <i class='fas fa-book-reader'></i>
                    Books</a>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <div class="mr-auto"></div>
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <?=Html::a('Home',['site/index'])?>
                            </li>
                            <li class="nav-item">
                                 <?=Html::a('About Us',['site/about'])?>
                            </li>
                            <li class="nav-item">
                               <?=Html::a('Our Services',['site/services'])?>
                            </li>
                            <li class="nav-item">
                                <?=Html::a('Contact Us',['site/contact'])?>
                            </li>
                            <li class="nav-item">
                                <?=Html::a('Happy Clients',['site/clients'])?>
                            </li>
                            
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-7 col-xs-7 col-sm-12">
                        <h6>AUTHOR: DAILY TUTION</h6>
                        <h1>EXCITING ADVENTURE</h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <button class="btn btn-light px-5 py-2">By now for $8.99</button>
                    </div>
                    <div class="col-md-5 col-xs-5 col-sm-12">
                        <img src="Images/book.jpg" alt="Books" style="height: 250px;">
                    </div>
                </div>
            </div>

          
            
        </header>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer >
    <div class="container-fluid p-0">
        <div class="row text-left">
            <div class="col-md-5 col-md-5">
                <h1 class="text-light">About Us</h1>
                <p class="text-muted">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <p class="text-muted">
                    Copyright @2019 All rights reserved | 
                    <span>Books.edu.np</span>
                </p>
            </div>
            <div class="col-md-5">
                <h4 class="text-muted">Newsletter</h4>
                <p class="text-muted">Stay Updated</p>
                <form class="form-inline">
                <div class="col pl-">
                    <div class="input-group pr-5">
                        <input type="text" class="form-control bg-dark text-white" placeholder="Email">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class='fas fa-arrow-right' style='font-size:24px; color: dodgerblue;'></i>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>                  
                </form>
                
            </div>
            <div class="col-md-2 col-sm-12">
                <h4 class="text-light">Follow Us</h4>
                <p class="text-muted">Let us be social</p>
                <div class="column">
                    <i class="fa fa-facebook-f"></i>
                    <i class="fa fa-instagram"></i>
                    <i class="fa fa-twitter"></i>
                    <i class="fa fa-youtube"></i>
                    
                </div>
                
            </div>
            
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
