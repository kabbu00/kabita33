 		<main>
 			<section class="section-1">
 				<div class="container text-center">
 					<div class="row">
 						<div class="col-md-6 col-sm-12">
 							<div class="pray">
 								<img src="Images/7.jpg" alt="Pray">
 							</div>
 						</div>
 						<div class="col-md-6 col-sm-12">
 							<div class="panel text-left">
 								<h1>Mr. Grant Smith</h1>
 								<p  class="pt-4">
 									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 									consequat. 
 								</p>
 								<p>
 									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 									consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 									cillum dolore eu fugiat nulla pariatur.Duis aute irure dolor in reprehenderit in voluptate velit esse
 									cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 									proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 								</p>
 							</div>	

 						</div>					
 					</div>
 					
 				</div>
 				
 			</section>
 			<section class="section-2 container-fluid p-0">
 				<div class="cover">
 					<div class="content text-center">
 						
 						
 						<h1>Some Features That Made Us Unique</h1>
 						<p>
 							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 						</p>
 						
 					</div>
 					
 				</div>

 				<div class="container-fluid text-center">
 					<div class="numbers d-flex flex-md-row flex-wrap justify-content-center">
 						<div class="rect">
 							<h1>1234</h1>
 							<p>Happy Client</p>
 						</div>
 						<div class="rect">
 							<h1>6789</h1>
 							<p>Cups Coffee</p>
 						</div>
 						<div class="rect">
 							<h1>4567</h1>
 							<p>Tickets Submitted</p>
 						</div>
 						<div class="rect">
 							<h1>2345</h1>
 							<p>Total Projects</p>
 						</div>
 					</div>
 					
 				</div>
 				<div class="purchase text-center">
 					<h1>purchase Whatever you Want</h1>
 					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 					<div class="cards">
 						<div class="d-flex flex-row justify-content-center flex-wrap">

 							<!--First Card-->
 							<div class="card">
 								<div class="card-body">
 									<div class="title">
 										<h5 class="card-title">PDF</h5>

 									</div>
 									<p class="card-text">
 										With supporting text below as a natural lead-in.
 									</p>
 									<div class="pricing">
 										<h1>$77.99</h1>
 										<a href="#" class="btn btn-dark px-5py-2 mb-5">Purchase Now</a>

 									</div>
 								</div>

 							</div>
 							<!--second Card-->
 							<div class="card">
 								<div class="card-body">
 									<div class="title">
 										<h5 class="card-title">E-book</h5>

 									</div>
 									<p class="card-text">
 										With supporting text below as a natural lead-in.
 									</p>
 									<div class="pricing">
 										<h1>$99.99</h1>
 										<a href="#" class="btn btn-dark px-5py-2 mb-5">Purchase Now</a>

 									</div>
 								</div>

 							</div>
 							<!--Third Card-->
 							<div class="card">
 								<div class="card-body">
 									<div class="title">
 										<h5 class="card-title">Print copy</h5>

 									</div>
 									<p class="card-text">
 										With supporting text below as a natural lead-in.
 									</p>
 									<div class="pricing">
 										<h1>$55.99</h1>
 										<a href="#" class="btn btn-dark px-5py-2 mb-5">Purchase Now</a>

 									</div>
 								</div>

 							</div>
 						</div>
 					</div>
 				</div>
 			</section>
 			<section class="section-3 container-fluid p-0 text-center">
 				<div class="row">
 					<div class="col-md-12 col-sm-12">
 						<h1>Download Our App For all platforms</h1>
 						<p >Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 						</p>

 					</div>
 				</div>
 				<div class="platform row">
 					<div class="col-md-6 col-sm-12 text-right">
 						<div class="desktop shadow-lg">
 							<div class="d-flex flex-row justify-content-center">
 								<i class="fas fa-desktop fa-3x py-2 pr-3"></i>
 								<div class="text text-left">
 									<h3 class="pt-1 m-0">Desktop</h3>
 									<p class="p-0 m-0">On Website</p>
 								</div>

 							</div>

 						</div>
 					</div>
 					<div class="col-md-6 col-sm-12 text-left">
 						<div class="desktop shadow-lg">
 							<div class="d-flex flex-row justify-content-center">
 								<i class="fa fa-mobile" aria-hidden="true" style="font-size: 36px;"></i>
 								<div class="text text-left">
 									<h3 class="pt-1 m-0">On Mobile</h3>
 									<p class="p-0 m-0">On playstore</p>
 								</div>

 							</div>

 						</div>

 					</div>
 				</div>
 			</section>
 			<section class="section-4">
 				<div class="container text-center">
 					<h1 class="text-dark">What our Reader's Say About us</h1>
 					<p>Lorem ipsum dolor sit amet.</p>

 				</div>
 				<div class="team row">
 					<div class="col-md-4 col-12 text-center">
 						<div class="card mr-2 d-inline-block shadow-lg">
 							<div class="card-img-top">
 								<img src="Images/1.JPG" alt="member" class="img-fluid border-radius p-4">
 							</div>
 							<div class="card-body">
 								<h3 class="card-title">Blalock Jolene</h3>
 								<p class="card-text">
 									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 									consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 									cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 									proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 								</p>
 								<a href="#" class="text-secondary text-decoration-none">Go Somewhere</a>
 								<p class="text-black-50">CEO at Google</p>
 							</div>
 						</div>

 					</div>
 					<div class="col-md-4 col-12 text-center">
 						<!--card2-->
 						<div class="card mr-2 d-inline-block shadow-lg">
 							<div class="card-img-top">
 								<img src="Images/2.JPG" alt="member" class="img-fluid rounded-circle p-4">
 							</div>
 							<div class="card-body">
 								<h3 class="card-title">Allen Agnes</h3>
 								<p class="card-text">
 									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 									consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 									cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 									proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 								</p>
 								<a href="#" class="text-secondary text-decoration-none">Go Somewhere</a>
 								<p class="text-black-50">CEO at Google</p>
 							</div>
 						</div>
 					</div>		

 					
 					<div class="col-md-4 col-12 text-center">
 						<!--card3-->
 						<div class="card mr-2 d-inline-block shadow-lg">
 							<div class="card-img-top">
 								<img src="Images/4.JPG" alt="member" class="img-fluid border-radius p-4">
 							</div>
 							<div class="card-body">
 								<h3 class="card-title">Olivia Louis</h3>
 								<p class="card-text">
 									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 									consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 									cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 									proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 								</p>
 								<a href="#" class="text-secondary text-decoration-none">Go Somewhere</a>
 								<p class="text-black-50">CEO at Google</p>
 							</div>
 						</div>
 						
 					</div>

 				</div>

 			</section>
 		</main>