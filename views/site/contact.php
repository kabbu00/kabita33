<main>
            <div class="maincontact">
                <center>
                    <div class="contact">Contact Us</div>
                </center>

                <div class="row">

                    <div class="col-md-4 col-xs-4 col-sm-12">
                        <center>
                            <img src="Images/c1.jpg" class="c">
                            <h2 class="c1"> By Phone</h2>
                            <div>(Monday to Saturday; 11pm to 4pm )</div>
                            <div class="c2">
                            Contact No: 9808892646
                                        9807546789
                            </div>
                        </center>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-12">
                        <center>
                            <img src="Images/c2.jpg" class="c">
                            <h2 class="c1"> Start a new case</h2>
                            <p class="c2">
                                Just send us your questions or concerns by starting 
                                a new case and we will give you the help you need.
                            </p>
                            <div class="c3">
                                <button type="start" class="btn">Start Here</button>
                            </div>
                        </center>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-12">
                        <center>
                            <img src="Images/c3.jpg" class="c">
                            <h2 class="c1"> Live chat</h2>
                            <p class="c2">
                                Chat with a member of our in-house team.
                            </p>
                            <div class="c3">
                                <button type="start" class="btn">Start Chat</button>
                            </div>
                        </center>
                    </div>
                    
                </div>

            </div>

            <div>
                <center>
                    <div class="contact">Track a Case</div>
                        <p class="c4">View your thread of messages with our support team.</p>
                        <div class="container contact-form">
                            <div class="contact-image">
                                <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
                            </div>
                            <form method="post">
                                <h3>Drop Us a Message</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="txtName" class="form-control" placeholder="Your Name *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="txtEmail" class="form-control" placeholder="Your Email *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="txtPhone" class="form-control" placeholder="Your Phone Number *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" name="btnSubmit" class="btnContact" value="Send Message" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <textarea name="txtMsg" class="form-control" placeholder="Your Message *" style="width: 100%; height: 150px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                     
                    </div>

                </center>

            </div>
        
        </main>